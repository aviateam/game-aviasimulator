﻿namespace Game
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader columnFlight;
            System.Windows.Forms.ColumnHeader columnPlane;
            System.Windows.Forms.ColumnHeader columnDate;
            System.Windows.Forms.ColumnHeader columnTime;
            System.Windows.Forms.ColumnHeader columnName;
            System.Windows.Forms.ColumnHeader columnType;
            this.menuPanel = new System.Windows.Forms.MenuStrip();
            this.menuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadButton = new System.Windows.Forms.ToolStripMenuItem();
            this.saveButton = new System.Windows.Forms.ToolStripMenuItem();
            this.exitButton = new System.Windows.Forms.ToolStripMenuItem();
            this.управлениеСамолётамиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flightBox = new System.Windows.Forms.ListView();
            this.columnDistance = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = new System.Windows.Forms.GroupBox();
            this.stopTimeButton = new System.Windows.Forms.Button();
            this.normalTimeButton = new System.Windows.Forms.Button();
            this.quickTimeButton = new System.Windows.Forms.Button();
            this.TimeValue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.moneyValue = new System.Windows.Forms.Label();
            this.moneyLabel = new System.Windows.Forms.Label();
            this.timeTable = new System.Windows.Forms.ListView();
            this.columnTypeFlight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.takeFlight = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnFlight2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnType2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMaxDate2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDisctansion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPrice2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMass2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button2 = new System.Windows.Forms.Button();
            columnFlight = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnPlane = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.menuPanel.SuspendLayout();
            this.Status.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnFlight
            // 
            columnFlight.Text = "Рейс";
            columnFlight.Width = 100;
            // 
            // columnPlane
            // 
            columnPlane.Text = "Самолёт";
            columnPlane.Width = 111;
            // 
            // columnDate
            // 
            columnDate.Text = "Дата вылета";
            columnDate.Width = 126;
            // 
            // columnTime
            // 
            columnTime.Text = "Время вылета";
            columnTime.Width = 101;
            // 
            // columnName
            // 
            columnName.Text = "Рейс";
            columnName.Width = 89;
            // 
            // columnType
            // 
            columnType.Text = "Тип";
            columnType.Width = 75;
            // 
            // menuPanel
            // 
            this.menuPanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem,
            this.управлениеСамолётамиToolStripMenuItem});
            this.menuPanel.Location = new System.Drawing.Point(0, 0);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(1144, 24);
            this.menuPanel.TabIndex = 1;
            this.menuPanel.Text = "menuStrip1";
            // 
            // menuItem
            // 
            this.menuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadButton,
            this.saveButton,
            this.exitButton});
            this.menuItem.Name = "menuItem";
            this.menuItem.Size = new System.Drawing.Size(53, 20);
            this.menuItem.Text = "Меню";
            // 
            // loadButton
            // 
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(132, 22);
            this.loadButton.Text = "Загрузить";
            // 
            // saveButton
            // 
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(132, 22);
            this.saveButton.Text = "Сохранить";
            // 
            // exitButton
            // 
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(132, 22);
            this.exitButton.Text = "Выход";
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // управлениеСамолётамиToolStripMenuItem
            // 
            this.управлениеСамолётамиToolStripMenuItem.Name = "управлениеСамолётамиToolStripMenuItem";
            this.управлениеСамолётамиToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.управлениеСамолётамиToolStripMenuItem.Text = "Мои самолёты";
            // 
            // flightBox
            // 
            this.flightBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            columnName,
            columnType,
            this.columnDistance,
            this.columnPrice,
            this.columnMass});
            this.flightBox.Location = new System.Drawing.Point(715, 100);
            this.flightBox.Name = "flightBox";
            this.flightBox.Size = new System.Drawing.Size(417, 569);
            this.flightBox.TabIndex = 2;
            this.flightBox.UseCompatibleStateImageBehavior = false;
            this.flightBox.View = System.Windows.Forms.View.Details;
            // 
            // columnDistance
            // 
            this.columnDistance.Text = "Дальность";
            this.columnDistance.Width = 72;
            // 
            // columnPrice
            // 
            this.columnPrice.Text = "Оплата";
            this.columnPrice.Width = 58;
            // 
            // columnMass
            // 
            this.columnMass.Text = "Масса";
            this.columnMass.Width = 61;
            // 
            // Status
            // 
            this.Status.Controls.Add(this.stopTimeButton);
            this.Status.Controls.Add(this.normalTimeButton);
            this.Status.Controls.Add(this.quickTimeButton);
            this.Status.Controls.Add(this.TimeValue);
            this.Status.Controls.Add(this.label1);
            this.Status.Controls.Add(this.moneyValue);
            this.Status.Controls.Add(this.moneyLabel);
            this.Status.Location = new System.Drawing.Point(715, 27);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(348, 67);
            this.Status.TabIndex = 3;
            this.Status.TabStop = false;
            this.Status.Text = "Информация";
            // 
            // stopTimeButton
            // 
            this.stopTimeButton.Location = new System.Drawing.Point(244, 13);
            this.stopTimeButton.Name = "stopTimeButton";
            this.stopTimeButton.Size = new System.Drawing.Size(26, 23);
            this.stopTimeButton.TabIndex = 6;
            this.stopTimeButton.Text = "■";
            this.stopTimeButton.UseVisualStyleBackColor = true;
            // 
            // normalTimeButton
            // 
            this.normalTimeButton.Location = new System.Drawing.Point(276, 13);
            this.normalTimeButton.Name = "normalTimeButton";
            this.normalTimeButton.Size = new System.Drawing.Size(26, 23);
            this.normalTimeButton.TabIndex = 5;
            this.normalTimeButton.Text = ">";
            this.normalTimeButton.UseVisualStyleBackColor = true;
            // 
            // quickTimeButton
            // 
            this.quickTimeButton.Location = new System.Drawing.Point(308, 13);
            this.quickTimeButton.Name = "quickTimeButton";
            this.quickTimeButton.Size = new System.Drawing.Size(34, 23);
            this.quickTimeButton.TabIndex = 4;
            this.quickTimeButton.Text = ">>";
            this.quickTimeButton.UseVisualStyleBackColor = true;
            // 
            // TimeValue
            // 
            this.TimeValue.AutoSize = true;
            this.TimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TimeValue.Location = new System.Drawing.Point(78, 16);
            this.TimeValue.Name = "TimeValue";
            this.TimeValue.Size = new System.Drawing.Size(43, 20);
            this.TimeValue.TabIndex = 3;
            this.TimeValue.Text = "Time";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Время:";
            // 
            // moneyValue
            // 
            this.moneyValue.AutoSize = true;
            this.moneyValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.moneyValue.Location = new System.Drawing.Point(84, 36);
            this.moneyValue.Name = "moneyValue";
            this.moneyValue.Size = new System.Drawing.Size(97, 20);
            this.moneyValue.TabIndex = 1;
            this.moneyValue.Text = "moneyValue";
            // 
            // moneyLabel
            // 
            this.moneyLabel.AutoSize = true;
            this.moneyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.moneyLabel.Location = new System.Drawing.Point(10, 36);
            this.moneyLabel.Name = "moneyLabel";
            this.moneyLabel.Size = new System.Drawing.Size(68, 20);
            this.moneyLabel.TabIndex = 0;
            this.moneyLabel.Text = "Деньги:";
            // 
            // timeTable
            // 
            this.timeTable.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            columnFlight,
            this.columnTypeFlight,
            columnPlane,
            columnDate,
            columnTime});
            this.timeTable.Location = new System.Drawing.Point(12, 27);
            this.timeTable.Name = "timeTable";
            this.timeTable.Size = new System.Drawing.Size(697, 288);
            this.timeTable.TabIndex = 4;
            this.timeTable.UseCompatibleStateImageBehavior = false;
            this.timeTable.View = System.Windows.Forms.View.Details;
            // 
            // columnTypeFlight
            // 
            this.columnTypeFlight.Text = "Тип";
            this.columnTypeFlight.Width = 91;
            // 
            // takeFlight
            // 
            this.takeFlight.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.takeFlight.Location = new System.Drawing.Point(1069, 31);
            this.takeFlight.Name = "takeFlight";
            this.takeFlight.Size = new System.Drawing.Size(63, 63);
            this.takeFlight.TabIndex = 5;
            this.takeFlight.Text = "+";
            this.takeFlight.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Location = new System.Drawing.Point(13, 322);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(696, 347);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Взятые рейсы";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(505, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 35);
            this.button1.TabIndex = 1;
            this.button1.Text = "Назначить самолёт";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnFlight2,
            this.columnType2,
            this.columnMaxDate2,
            this.columnDisctansion,
            this.columnPrice2,
            this.columnMass2});
            this.listView1.Location = new System.Drawing.Point(7, 20);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(491, 321);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnFlight2
            // 
            this.columnFlight2.Text = "Рейс";
            this.columnFlight2.Width = 77;
            // 
            // columnType2
            // 
            this.columnType2.Text = "Тип";
            this.columnType2.Width = 78;
            // 
            // columnMaxDate2
            // 
            this.columnMaxDate2.Text = "Крайний срок";
            this.columnMaxDate2.Width = 93;
            // 
            // columnDisctansion
            // 
            this.columnDisctansion.Text = "Растояние";
            this.columnDisctansion.Width = 82;
            // 
            // columnPrice2
            // 
            this.columnPrice2.Text = "Оплата";
            this.columnPrice2.Width = 74;
            // 
            // columnMass2
            // 
            this.columnMass2.Text = "Масса";
            this.columnMass2.Width = 81;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(505, 61);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(185, 159);
            this.button2.TabIndex = 2;
            this.button2.Text = "master жив!!!";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 681);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.takeFlight);
            this.Controls.Add(this.timeTable);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.flightBox);
            this.Controls.Add(this.menuPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuPanel;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AviaSimulator";
            this.menuPanel.ResumeLayout(false);
            this.menuPanel.PerformLayout();
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuPanel;
        private System.Windows.Forms.ToolStripMenuItem menuItem;
        private System.Windows.Forms.ToolStripMenuItem loadButton;
        private System.Windows.Forms.ToolStripMenuItem saveButton;
        private System.Windows.Forms.ToolStripMenuItem exitButton;
        private System.Windows.Forms.ToolStripMenuItem управлениеСамолётамиToolStripMenuItem;
        private System.Windows.Forms.ListView flightBox;
        private System.Windows.Forms.GroupBox Status;
        private System.Windows.Forms.Label TimeValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label moneyValue;
        private System.Windows.Forms.Label moneyLabel;
        private System.Windows.Forms.Button stopTimeButton;
        private System.Windows.Forms.Button normalTimeButton;
        private System.Windows.Forms.Button quickTimeButton;
        private System.Windows.Forms.ListView timeTable;
        private System.Windows.Forms.ColumnHeader columnDistance;
        private System.Windows.Forms.ColumnHeader columnPrice;
        private System.Windows.Forms.ColumnHeader columnMass;
        private System.Windows.Forms.ColumnHeader columnTypeFlight;
        private System.Windows.Forms.Button takeFlight;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnFlight2;
        private System.Windows.Forms.ColumnHeader columnType2;
        private System.Windows.Forms.ColumnHeader columnMaxDate2;
        private System.Windows.Forms.ColumnHeader columnDisctansion;
        private System.Windows.Forms.ColumnHeader columnPrice2;
        private System.Windows.Forms.ColumnHeader columnMass2;
        private System.Windows.Forms.Button button2;
    }
}

